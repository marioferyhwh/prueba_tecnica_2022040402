export class PaisModel {
  constructor(
    public pais_codigo: number,
    public pais_nombre: string,
    public pais_dominio: string
  ) {}
}
