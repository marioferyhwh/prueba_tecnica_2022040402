import express from "express";
import { areaRouter } from "./areaRouter";
import { empleadoRouter } from "./empleadoRouter";
import { paisRouter } from "./paisRouter";
import { tipoDocumentoRouter } from "./tipoDocumentoRouter";

const url = "/api/v1/";

export function routerApi(app) {
  app.get("/", (req: express.Request, res: express.Response) => {
    res.send("--- hola ---");
  });

  const router = express.Router();
  app.use("/api/v1", router);
  router.use(`/area`, areaRouter);
  router.use(`/pais`, paisRouter);
  router.use(`/tipodocumento`, tipoDocumentoRouter);
  router.use(`/empleado`, empleadoRouter);
}
