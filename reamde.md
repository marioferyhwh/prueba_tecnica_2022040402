# BackendApp1

## INSTALL

`npm install`

## Deploy server

Run `npm run start` for a dev server. Navigate to `http://localhost:3000/`.

## BreakPoint

`GET api/v1/area`

respuesta

```js
{
    "code": 200,
    "message": "",
    "data": [
        {
            "area_codigo": 1,
            "area_nombre": "Administracion"
        },
        {
            "area_codigo": 2,
            "area_nombre": "Financiera"
        },
        {
            "area_codigo": 3,
            "area_nombre": "Compras"
        },
        {
            "area_codigo": 4,
            "area_nombre": "Infraestructura"
        },
        {
            "area_codigo": 5,
            "area_nombre": "Operacion"
        },
        {
            "area_codigo": 6,
            "area_nombre": "Talento Humano"
        },
        {
            "area_codigo": 7,
            "area_nombre": "Servicios Varios"
        }
    ]
}
```

`GET api/v1/pais`

respuesta

```js
{
    "code": 200,
    "message": "",
    "data": [
        {
            "pais_codigo": 1,
            "pais_nombre": "Colombia",
            "pais_dominio": "cidenet.com.co"
        },
        {
            "pais_codigo": 2,
            "pais_nombre": "Estados Unidos",
            "pais_dominio": "cidenet.com.us"
        }
    ]
}
```

`GET api/v1/tipodocumento`

respuesta

```js
{
    "code": 200,
    "message": "",
    "data": [
        {
            "tipodocumento_codigo": 1,
            "tipodocumento_nombre": "cedula de ciudadania"
        },
        {
            "tipodocumento_codigo": 2,
            "tipodocumento_nombre": "cedula de estrangeria"
        },
        {
            "tipodocumento_codigo": 3,
            "tipodocumento_nombre": "pasaporte"
        },
        {
            "tipodocumento_codigo": 4,
            "tipodocumento_nombre": "permiso especial"
        }
    ]
}
```

`POST api/v1/empleado`

request

```js
{
	"empleado_primer_nombre": " mario ",
	"empleado_otro_nombre": "",
	"empleado_primer_apellido": "fernando",
	"empleado_segundo_apellido": "robayo",
	"pais_codigo": 1,
	"tipodocumento_codigo": 1,
	"empleado_identificacion": "1111",
	"area_codigo": 1,
	"empleado_ingreso": "2022-04-01"
}
```

respuesta

```js
{
    "code": 200,
    "message": "",
    "data": {
        "empleado_codigo": 3,
        "empleado_primer_nombre": "mario",
        "empleado_otro_nombre": "",
        "empleado_primer_apellido": "fernando",
        "empleado_segundo_apellido": "robayo",
        "pais_codigo": 1,
        "tipodocumento_codigo": 1,
        "empleado_identificacion": "1111",
        "empleado_correo": "mario.fernando@cidenet.com.co",
        "area_codigo": 1,
        "empleado_estado": true,
        "empleado_ingreso": "2022-04-01",
        "empleado_creado": "2022-04-05",
        "empleado_modificado": "2022-04-05"
    }
}
```

`GET api/v1/empleado`

respuesta

```js
{
    "code": 200,
    "message": "",
    "data": [
        {
            "empleado_codigo": 1,
            "empleado_primer_nombre": "mar",
            "empleado_otro_nombre": "amr",
            "empleado_primer_apellido": "mar",
            "empleado_segundo_apellido": "mar",
            "pais_codigo": "1",
            "tipodocumento_codigo": "1",
            "empleado_identificacion": "sadasd",
            "empleado_correo": "mar.mar@cidenet.com.co",
            "area_codigo": "1",
            "empleado_estado": true,
            "empleado_ingreso": "2022-04-01",
            "empleado_creado": "2022-04-05",
            "empleado_modificado": "2022-04-05"
        },
        {
            "empleado_codigo": 2,
            "empleado_primer_nombre": "mar",
            "empleado_otro_nombre": "sda",
            "empleado_primer_apellido": "mar",
            "empleado_segundo_apellido": "sad",
            "pais_codigo": "1",
            "tipodocumento_codigo": "3",
            "empleado_identificacion": "sad",
            "empleado_correo": "mar.mar.1@cidenet.com.co",
            "area_codigo": "1",
            "empleado_estado": true,
            "empleado_ingreso": "2022-03-31",
            "empleado_creado": "2022-04-05",
            "empleado_modificado": "2022-04-05"
        }
    ]
}
```

`GET api/v1/empleado/:id`
respuesta

```js
{
	"code": 200,
	"message": "",
	"data": {
			"empleado_codigo": 1,
			"empleado_primer_nombre": "mar",
			"empleado_otro_nombre": "amr",
			"empleado_primer_apellido": "mar",
			"empleado_segundo_apellido": "mar",
			"pais_codigo": "1",
			"tipodocumento_codigo": "1",
			"empleado_identificacion": "sadasd",
			"empleado_correo": "mar.mar@cidenet.com.co",
			"area_codigo": "1",
			"empleado_estado": true,
			"empleado_ingreso": "2022-04-01",
			"empleado_creado": "2022-04-05",
			"empleado_modificado": "2022-04-05"
	}
}
```

`PUT api/v1/empleado/:id`

request

```js
{
	"empleado_codigo": 1,
	"empleado_primer_nombre": " mario ",
	"empleado_otro_nombre": "",
	"empleado_primer_apellido": "fernando",
	"empleado_segundo_apellido": "robayo",
	"pais_codigo": 1,
	"tipodocumento_codigo": 1,
	"empleado_identificacion": "1111",
	"area_codigo": 1,
	"empleado_ingreso": "2022-04-01"
}
```

respuesta

```js
{
    "code": 200,
    "message": "",
    "data": {
        "empleado_codigo": 1,
        "empleado_primer_nombre": "mar",
        "empleado_otro_nombre": "amr",
        "empleado_primer_apellido": "mar",
        "empleado_segundo_apellido": "mar",
        "pais_codigo": "1",
        "tipodocumento_codigo": "1",
        "empleado_identificacion": "sadasd",
        "empleado_correo": "mar.mar@cidenet.com.co",
        "area_codigo": "1",
        "empleado_estado": true,
        "empleado_ingreso": "2022-04-01",
        "empleado_creado": "2022-04-05",
        "empleado_modificado": "2022-04-05"
    }
}
```

`DELETE api/v1/empleado/:id`

respuesta

```js
{
    "code": 200,
    "message": ""
}
```
