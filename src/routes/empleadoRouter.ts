import express from "express";
import { RespModel } from "../shared/models/resp-model";
import { EmpleadoController } from "../Controllers/empleadoController";

const router = express.Router();

router.get(`/`, (req: express.Request, res: express.Response) => {
  const resp = new RespModel(200, "");
  EmpleadoController.getList(req, resp);
  res.status(resp.code).send(resp);
});

router.get(`/:id`, (req: express.Request, res: express.Response) => {
  const resp = new RespModel(200, "");
  EmpleadoController.get(req, resp);
  res.status(resp.code).send(resp);
});

router.post(`/`, (req: express.Request, res: express.Response) => {
  const resp = new RespModel(200, "");
  EmpleadoController.post(req, resp);
  res.status(resp.code).send(resp);
});

router.put(`/:id`, (req: express.Request, res: express.Response) => {
  const resp = new RespModel(200, "");
  EmpleadoController.update(req, resp);
  res.status(resp.code).send(resp);
});

router.delete(`/:id`, (req: express.Request, res: express.Response) => {
  const resp = new RespModel(200, "");
  EmpleadoController.delete(req, resp);
  res.status(resp.code).send(resp);
});

export { router as empleadoRouter };
