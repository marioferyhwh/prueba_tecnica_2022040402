import express from "express";
import { PaisController } from "../Controllers/paisController";
import { RespModel } from "../shared/models/resp-model";

const router = express.Router();

router.get(`/`, (req: express.Request, res: express.Response) => {
  const resp = new RespModel(200, "");
  PaisController.getList(req, resp);
  res.status(resp.code).send(resp);
});

router.get(`/:id`, (req: express.Request, res: express.Response) => {
  const resp = new RespModel(200, "");
  PaisController.get(req, resp);
  res.status(resp.code).send(resp);
});

export { router as paisRouter };
