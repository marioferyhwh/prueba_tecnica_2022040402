import { AreaModel } from "../shared/models/area-model";
import { RespModel } from "../shared/models/resp-model";
import express from "express";
import { TipoDocumentoModel } from "../shared/models/tipo-documento-model";
export class TipoDocumentoController {
  private static tipoDocumentoTest: TipoDocumentoModel[] = [
    new TipoDocumentoModel(1, "cedula de ciudadania"),
    new TipoDocumentoModel(2, "cedula de estrangeria"),
    new TipoDocumentoModel(3, "pasaporte"),
    new TipoDocumentoModel(4, "permiso especial"),
  ];
  public static getList = (req: express.Request, res: RespModel): void => {
    res.data = this.tipoDocumentoTest;
  };

  public static get = (req: express.Request, res: RespModel): void => {
    const { id } = req.params;
    if (!id) {
      res.code = 404;
      res.message = "Bad Request";
      return;
    }
    res.data = this.tipoDocumentoTest.find((x) => {
      return x.tipodocumento_codigo == id;
    });
  };
}
