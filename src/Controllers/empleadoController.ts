import { RespModel } from "../shared/models/resp-model";
import express from "express";
import { EmpleadoModel } from "../shared/models/empleado-model";
import { PaisController } from "./paisController";
export class EmpleadoController {
  private static empleadoAutoIncrement = 1;
  private static EmpleadoTest: EmpleadoModel[] = [];

  private static FormatDate(fecha: Date): string {
    return (
      fecha.getFullYear() +
      "-" +
      (fecha.getMonth() < 9 ? "0" : "") +
      (fecha.getMonth() + 1) +
      "-" +
      (fecha.getDate() < 10 ? "0" : "") +
      fecha.getDate()
    );
  }

  private static validator = (
    oldEmpleado: EmpleadoModel,
    newEmpleado: EmpleadoModel,
    nuevo: boolean = true
  ): boolean => {
    let change_correo: boolean = false;
    const text1 = new RegExp("^[a-zA-Z]{2,20}$");
    const text2 = new RegExp("^[a-zA-Zs]{0,50}$");
    const date1 = new RegExp("^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$");

    //primer
    newEmpleado.empleado_primer_nombre =
      newEmpleado.empleado_primer_nombre.trim();
    if (
      (newEmpleado.empleado_primer_nombre &&
        newEmpleado.empleado_primer_nombre !=
          oldEmpleado.empleado_primer_nombre) ||
      nuevo
    ) {
      if (!text1.test(newEmpleado.empleado_primer_nombre)) {
        console.error("error empleado_primer_nombre");
        return false;
      }
      change_correo = true;
      oldEmpleado.empleado_primer_nombre = newEmpleado.empleado_primer_nombre;
    }
    //apellido primer
    newEmpleado.empleado_primer_apellido =
      newEmpleado.empleado_primer_apellido.trim();
    if (
      (newEmpleado.empleado_primer_apellido &&
        newEmpleado.empleado_primer_apellido !=
          oldEmpleado.empleado_primer_apellido) ||
      nuevo
    ) {
      if (!text1.test(newEmpleado.empleado_primer_apellido)) {
        console.error("error empleado_primer_apellido");
        return false;
      }
      change_correo = true;
      oldEmpleado.empleado_primer_apellido =
        newEmpleado.empleado_primer_apellido;
    }
    //apellido segundo
    newEmpleado.empleado_segundo_apellido =
      newEmpleado.empleado_segundo_apellido.trim();
    if (
      (newEmpleado.empleado_segundo_apellido &&
        newEmpleado.empleado_segundo_apellido !=
          oldEmpleado.empleado_segundo_apellido) ||
      nuevo
    ) {
      if (!text1.test(newEmpleado.empleado_segundo_apellido)) {
        console.error("error empleado_segundo_apellido");
        return false;
      }
      oldEmpleado.empleado_segundo_apellido =
        newEmpleado.empleado_segundo_apellido;
    }
    // nombre otros
    newEmpleado.empleado_otro_nombre = newEmpleado.empleado_otro_nombre.trim();
    if (
      (newEmpleado.empleado_otro_nombre &&
        newEmpleado.empleado_otro_nombre != oldEmpleado.empleado_otro_nombre) ||
      nuevo
    ) {
      if (!text2.test(newEmpleado.empleado_otro_nombre)) {
        console.error("error empleado_otro_nombre");
        return false;
      }
      oldEmpleado.empleado_otro_nombre = newEmpleado.empleado_otro_nombre;
    }

    // pais
    if (
      (newEmpleado.pais_codigo &&
        newEmpleado.pais_codigo != oldEmpleado.pais_codigo) ||
      nuevo
    ) {
      if (newEmpleado.pais_codigo <= 0) {
        console.error("error pais_codigo");
        return false;
      }
      change_correo = true;
      oldEmpleado.pais_codigo = newEmpleado.pais_codigo;
    }
    //area
    if (
      (newEmpleado.area_codigo &&
        newEmpleado.area_codigo != oldEmpleado.area_codigo) ||
      nuevo
    ) {
      if (newEmpleado.area_codigo <= 0) {
        console.error("error area_codigo");
        return false;
      }
      oldEmpleado.area_codigo = newEmpleado.area_codigo;
    }
    //tipo
    if (
      (newEmpleado.tipodocumento_codigo &&
        newEmpleado.tipodocumento_codigo != oldEmpleado.tipodocumento_codigo) ||
      nuevo
    ) {
      if (newEmpleado.tipodocumento_codigo <= 0) {
        console.error("error tipodocumento_codigo");
        return false;
      }
      oldEmpleado.tipodocumento_codigo = newEmpleado.tipodocumento_codigo;
    }
    //documento
    if (
      (newEmpleado.empleado_identificacion &&
        newEmpleado.empleado_identificacion !=
          oldEmpleado.empleado_identificacion) ||
      nuevo
    ) {
      if (
        !newEmpleado.empleado_identificacion ||
        newEmpleado.empleado_identificacion.length > 20
      ) {
        console.error("error empleado_identificacion");
        return false;
      }
      oldEmpleado.empleado_identificacion = newEmpleado.empleado_identificacion;
    }
    //estado
    if (nuevo) {
      oldEmpleado.empleado_estado = true;
    }

    oldEmpleado.empleado_modificado = this.FormatDate(new Date());

    //solo cuando es nuevo
    if (nuevo) {
      if (oldEmpleado.empleado_codigo) {
        console.log("empado codigo ya existe");
        return false;
      }
      if (!date1.test(newEmpleado.empleado_ingreso)) {
        console.error("error empleado_ingreso");
        return false;
      }
      const dias =
        (new Date().getTime() -
          new Date(newEmpleado.empleado_ingreso).getTime()) /
        86400000;
      if (dias < 0 || dias > 30) {
        console.error("error empleado_ingreso fuera de rango");
        return false;
      }
      oldEmpleado.empleado_ingreso = newEmpleado.empleado_ingreso;
      oldEmpleado.empleado_codigo = this.empleadoAutoIncrement++;
      oldEmpleado.empleado_creado = oldEmpleado.empleado_modificado;
    } else {
      if (oldEmpleado.empleado_codigo != newEmpleado.empleado_codigo) {
        return false;
      }
    }
    if (change_correo) {
      this.changeEmail(oldEmpleado);
    }
    return true;
  };

  private static changeEmail(empleado: EmpleadoModel) {
    const nombreCorreo =
      empleado.empleado_primer_nombre + "." + empleado.empleado_primer_apellido;
    const dominioCorreo = "@" +(empleado.pais_codigo == 2 ?"cidenet.com.us":"cidenet.com.co");
    let nuevoCorreo = nombreCorreo + dominioCorreo;
    const ids: any[] = this.EmpleadoTest.filter(
      (x) =>
        x.empleado_correo.startsWith(nombreCorreo) &&
        x.empleado_correo.endsWith(dominioCorreo)
    )
      .map((x) => {
        let numero: string = x.empleado_correo.slice(
          nombreCorreo.length,
          x.empleado_correo.length - dominioCorreo.length
        );
        return numero == "" ? 0 : numero.startsWith(".") ? numero.slice(1) : -1;
      })
      .filter((x) => x >= 0);
    if (ids.length > 0) {
      const id_correo = "." + (Math.max(...ids) + 1);
      nuevoCorreo = nombreCorreo + id_correo + dominioCorreo;
    }
    empleado.empleado_correo = nuevoCorreo;
  }

  public static getList = (req: express.Request, res: RespModel): void => {
    res.data = this.EmpleadoTest;
  };

  public static get = (req: express.Request, res: RespModel): void => {
    const { id } = req.params;
    if (!id) {
      res.code = 404;
      res.message = "Bad Request";
      return;
    }
    res.data = this.EmpleadoTest.find((x) => x.empleado_codigo == id);
  };

  public static post = (req: express.Request, res: RespModel): void => {
    const tempEmpleado = new EmpleadoModel();
    if (!this.validator(tempEmpleado, { ...req.body })) {
      res.code = 404;
      res.data = { ...req.body };
      return;
    }
    this.EmpleadoTest.push(tempEmpleado);
    res.data = tempEmpleado;
  };

  public static update = (req: express.Request, res: RespModel): void => {
    const { id } = req.params;
    if (!id) {
      res.code = 404;
      res.message = "Bad Request";
      return;
    }
    const index = this.EmpleadoTest.findIndex((x) => x.empleado_codigo == id);
    if (index === -1) {
      res.code = 404;
      res.message = "Not found";
      return;
    }
    const tempEmpleado = {...this.EmpleadoTest[index]};
    if (!this.validator(tempEmpleado, { ...req.body }, false)) {
      res.code = 404;
      res.data = { ...req.body };
      return;
    }

    this.EmpleadoTest[index] = tempEmpleado;
    res.data = tempEmpleado;
  };

  public static delete = (req: express.Request, res: RespModel): void => {
    const { id } = req.params;
    if (!id) {
      res.code = 404;
      res.message = "Bad Request";
      return;
    }
    const index = this.EmpleadoTest.findIndex((x) => x.empleado_codigo == id);
    if (index > -1) {
      this.EmpleadoTest.splice(index, 1);
    }
    res.data = this.EmpleadoTest.find((x) => x.empleado_codigo == id);
  };
}
