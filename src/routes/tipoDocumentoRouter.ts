import express from "express";
import { TipoDocumentoController } from "../Controllers/tipoDocumentoController";
import { RespModel } from "../shared/models/resp-model";

const router = express.Router();

router.get(`/`, (req: express.Request, res: express.Response) => {
  const resp = new RespModel(200, "");
  TipoDocumentoController.getList(req, resp);
  res.status(resp.code).send(resp);
});

router.get(`/:id`, (req: express.Request, res: express.Response) => {
  const resp = new RespModel(200, "");
  TipoDocumentoController.get(req, resp);
  res.status(resp.code).send(resp);
});

export { router as tipoDocumentoRouter };
