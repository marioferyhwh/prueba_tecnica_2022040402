import { AreaModel } from "../shared/models/area-model";
import { RespModel } from "../shared/models/resp-model";
import express from "express";
export class AreaController {
  private static areaTest: AreaModel[] = [
    new AreaModel(1, "Administracion"),
    new AreaModel(2, "Financiera"),
    new AreaModel(3, "Compras"),
    new AreaModel(4, "Infraestructura"),
    new AreaModel(5, "Operacion"),
    new AreaModel(6, "Talento Humano"),
    new AreaModel(7, "Servicios Varios"),
  ];
  public static getList = (req: express.Request, res: RespModel): void => {
    res.data = this.areaTest;
  };

  public static get = (req: express.Request, res: RespModel): void => {
    const { id } = req.params;
    if (!id) {
      res.code = 404;
      res.message = "Bad Request";
      return;
    }
    res.data = this.areaTest.find((x) => x.area_codigo == id);
  };

  public static post = (req: express.Request, res: RespModel): void => {
    const body = req.body;
    res.data = body;
  };
}
