export class TipoDocumentoModel {
  constructor(
    public tipodocumento_codigo: number,
    public tipodocumento_nombre: string
  ) {}
}
