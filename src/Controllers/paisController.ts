import { AreaModel } from "../shared/models/area-model";
import { RespModel } from "../shared/models/resp-model";
import express from "express";
import { PaisModel } from "../shared/models/pais-model";
export class PaisController {
  private static paisTest: PaisModel[] = [
    new PaisModel(1, "Colombia", "cidenet.com.co"),
    new PaisModel(2, "Estados Unidos", "cidenet.com.us"),
  ];
  public static getList = (req: express.Request, res: RespModel): void => {
    res.data = this.paisTest;
  };

  public static get = (req: express.Request, res: RespModel): void => {
    const { id } = req.params;
    if (!id) {
      res.code = 404;
      res.message = "Bad Request";
      return;
    }
    res.data = this.paisTest.find((x) => x.pais_codigo == id);
  };
}
